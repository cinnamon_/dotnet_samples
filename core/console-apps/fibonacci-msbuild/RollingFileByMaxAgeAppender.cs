﻿using System;
using System.Configuration;
using System.IO;
using log4net.Appender;

namespace Hello
{
    public class RollingFileByMaxAgeAppender : RollingFileAppender
    {
        public RollingFileByMaxAgeAppender()
            : base()
        {
        }

        protected override void AdjustFileBeforeAppend()
        {
            base.AdjustFileBeforeAppend();
            var maxAgeRollBackups = Convert.ToInt32(ConfigurationManager.AppSettings["MaxAgeRollBackups"].ToString());

            foreach (string file in Directory.GetFiles(Path.GetDirectoryName(File), "*.log.*"))
            {
                //if (System.IO.File.GetLastWriteTime(file) < DateTime.Today.AddDays(-1 * maxAgeRollBackups))
                //    DeleteFile(file);

                if (System.IO.File.GetLastWriteTime(file) < DateTime.Now.AddMinutes(-1))
                    DeleteFile(file);
            }
        }
    }
}

